//
//  Temperature.swift
//  SimpleWeather
//
//  Created by Chirag Tailor on 03/06/2021.
//

import Foundation

struct Temperature: Codable {
    var min: Double
    var max: Double
}
