//
//  API+Extensions.swift
//  SimpleWeather
//
//  Created by Chirag Tailor on 02/06/2021.
//

import Foundation

extension API {
    
    // Leicester URL - http://api.openweathermap.org/data/2.5/onecall?lat=52.6520&lon=-1.1236&exclude=minutely&appid=5021f1792ee6575418d58cba8cf0f12a&units=metric
    
    static func getURLFor(lat: Double, lon: Double) -> String {
        return "\(Constants.API.BASE_URL)onecall?lat=\(lat)&lon=\(lon)&exclude=minutely&appid=\(API_KEY)&units=metric"
    }
    
}
