//
//  SimpleWeatherApp.swift
//  SimpleWeather
//
//  Created by Chirag Tailor on 02/06/2021.
//

import SwiftUI

@main
struct SimpleWeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
