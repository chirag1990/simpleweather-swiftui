//
//  WeatherDetail.swift
//  SimpleWeather
//
//  Created by Chirag Tailor on 03/06/2021.
//

import Foundation

struct WeatherDetail: Codable {
    var main: String
    var description: String
    var icon: String
}
