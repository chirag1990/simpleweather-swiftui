//
//  Constants.swift
//  SimpleWeather
//
//  Created by Chirag Tailor on 02/06/2021.
//

import Foundation

public enum Constants {
    
    public enum API {
        public static let BASE_URL = "https://api.openweathermap.org/data/2.5/"
    }
}
